package com.yzp.common;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;

/**
 * 
 * @author YINZHIPING(yzp531@163.com)
 * 
 * AndroidLearn @(#)BaseActivity.java
 * 
 * @version 1.0
 * 
 * @Copyright 2012-10-31 下午08:26:54
 */
public class BaseActivity extends Activity {

	public static List<Activity> activityList = new ArrayList<Activity>();

	public static Context mContext;

	private long exitTime = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mContext = this;
		activityList.add(this);
		CrashHandler crashHandler = CrashHandler.getInstance();
		crashHandler.init(mContext);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if ((System.currentTimeMillis() - exitTime) > 2000) {
				UIHelper.showMsg(getApplicationContext(), "再按一次退出程序");
				exitTime = System.currentTimeMillis();
			} else {
				for (int i = 0; i < activityList.size(); i++) {
					if (null != activityList.get(i)) {
						activityList.get(i).finish();
					}
				}
				System.exit(0);
			}
			return true;
		} else if (keyCode == KeyEvent.KEYCODE_HOME) {
			UIHelper.showMsg(getApplicationContext(), "请双击返回键退出");
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onAttachedToWindow()
	 */
	@Override
	public void onAttachedToWindow() {
		// 加此句可以屏幕HOME键
		// this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD);
		super.onAttachedToWindow();
	}

}
